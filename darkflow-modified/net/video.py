from utils.loader import create_loader
from time import time as timer
import time
import tensorflow as tf
import numpy as np
import sys
import cv2
import os




def predict_video(self):
    inp_path = self.FLAGS.video_in
    outp_path = self.FLAGS.video_out

    video_in_stream = cv2.VideoCapture(inp_path)
    video_heigth = int(video_in_stream.get(cv2.CAP_PROP_FRAME_HEIGHT))
    video_width = int(video_in_stream.get(cv2.CAP_PROP_FRAME_WIDTH))
    video_frames_count = int(video_in_stream.get(cv2.CAP_PROP_FRAME_COUNT))
    video_codec = cv2.VideoWriter_fourcc(*'H264')
    video_framerate = int(video_in_stream.get(cv2.CAP_PROP_FPS ))
    video_out_stream = cv2.VideoWriter(outp_path, video_codec, video_framerate, (video_width,video_heigth))

    self.say('Press [ESC] to quit demo')
    assert video_in_stream.isOpened(), \
        'Cannot capture source'
    assert video_out_stream.isOpened(), \
        'Cannot create writer'

    elapsed = int()
    start = timer()
    while video_in_stream.isOpened():
        _, frame = video_in_stream.read()
        preprocessed = self.framework.preprocess(frame)
        feed_dict = {self.inp: [preprocessed]}
        net_out = self.sess.run(self.out, feed_dict)[0]
        processed = self.framework.postprocess(net_out, frame, False)
        video_out_stream.write(processed)
        #cv2.imshow('', processed)
        elapsed += 1
        if elapsed % 5 == 0:
            sys.stdout.write('\r')
            sys.stdout.write('{0:3.3f} FPS'.format(
                elapsed / (timer() - start)))
            sys.stdout.write('  ')
            spented_time = time.gmtime(timer() - start)
            sys.stdout.write('Frames elapsed: ' + str(elapsed) + ' of ' + str(video_frames_count))
            sys.stdout.write('  ')
            spented_time = time.gmtime(timer() - start)
            sys.stdout.write('Time spend: ' + time.strftime('%H:%M:%S', spented_time))
            sys.stdout.write('  ')
            frames_to_elaps = video_frames_count - elapsed
            remain_time = time.gmtime((timer() - start) / elapsed * frames_to_elaps)
            sys.stdout.write('Time remain: ' + time.strftime('%H:%M:%S', remain_time))
            sys.stdout.write('  ')
            sys.stdout.flush()
        choice = cv2.waitKey(1)
        if choice == 27: break

    video_in_stream.release()
    video_out_stream.release()
